import scrapy
from w3lib.html import remove_tags, remove_tags_with_content

class QuotesSpider(scrapy.Spider):
    name = "blog"

    def start_requests(self):
        urls = [
            'http://www.getingetpaid.com/blog-2/page/1/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for quote in response.css('#main article'):
            href=quote.css('a::attr(href)').extract_first()
            yield response.follow(href, self.parse_author)
        next_page = response.css('.nav-previous a::attr(href)').extract_first()
        yield response.follow(next_page, self.parse)


    def parse_author(self, response):
        content = response.xpath('//div[@class="entry-content"]//text()').extract()
        content = ' '.join(content)[70:]
        content = content[:-20].replace('(adsbygoogle = window.adsbygoogle || []).push({});','')

        yield{
            'title': response.xpath('//h1[@class="entry-title"]//text()').extract_first(),
            'content': content,
            'category': response.xpath('//span[@class="cat-links"]//a//text()').extract(),
            'author':'Maya Mundell'
        }
